/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file line_split.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 13, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#include "line_split.h"
#include <sstream>

// used from http://stackoverflow.com/questions/236129/splitting-a-string-in-c
std::vector<std::string> split_string(std::string str)
{
    std::string buf; 							//initiate the buffer string
	std::stringstream stringStream(str);	 	//Insert the string into a stream
	std::vector<std::string> tokens; 			//Use a vector to hold the words
    while (stringStream >> buf){
        tokens.push_back(buf);
    }
    return tokens;								//Return Vector with tokens for each word (split at empty spaces)
}

