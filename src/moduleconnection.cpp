/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file moduleconnection.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 11, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#include "moduleconnection.h"

ModuleConnection::ModuleConnection():
	mThisDevice("raspberry"),
	mOtherDevice("raspberry"),
	mMiddleWare("zeromq"){ }

ModuleConnection::ModuleConnection(ModulePort thisPort,ModuleKey thisKey,ModulePort otherPort,ModuleKey otherKey){
	ModuleConnection();
	mThisPort = thisPort;
	mThisModuleKey = thisKey;
	mOtherPort = otherPort;
	mOtherModuleKey = otherKey;
}

ModuleConnection::ModuleConnection(ModulePort thisPort,ModuleKey thisKey,
		ModulePort otherPort,ModuleKey otherKey,std::string middleware){
//	ModuleConnection(thisPort,thisKey,otherPort,otherKey);
	ModuleConnection();
	mThisPort = thisPort;
	mThisModuleKey = thisKey;
	mOtherPort = otherPort;
	mOtherModuleKey = otherKey;
	mMiddleWare = middleware;
}

ModuleConnection::ModuleConnection(ModulePort thisPort,ModuleKey thisKey,std::string thisDevice,ModulePort otherPort,ModuleKey otherKey,std::string otherDevice,std::string middleware){
	mThisPort = thisPort;
	mThisModuleKey = thisKey;
	mThisDevice = thisDevice;
	mOtherPort = otherPort;
	mOtherModuleKey = otherKey;
	mOtherDevice = otherDevice;
	mMiddleWare = middleware;
}

ModuleConnection::~ModuleConnection(){ }

void ModuleConnection::setMiddleWare(std::string middleware){
	mMiddleWare = middleware;
}

void ModuleConnection::Connect(){
	std::string command = "aimconnect " + mMiddleWare + " " + mThisModuleKey.getName() + " " +
			std::to_string(mThisModuleKey.getID())	+ " " + mThisPort.getDirection() + mOtherModuleKey.getName() + " " +
			std::to_string(mOtherModuleKey.getID()) + " " + mOtherPort.getDirection();
	std::system(command.c_str());
}

void ModuleConnection::Disconnect(){
	//TODO: as of yet, there is no aimdisconnect or something like that
}
