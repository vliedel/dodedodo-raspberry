/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief
 * @file dodedodo.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    Marc Hulscher
 * @date      Jun 14, 2013
 * @project   Dodedodo
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
  */

#include "dodedodo.h"

ModuleManager::ModuleManager():
mYarpServRunning(true),	//assumed to be running
mZMQServRunning(true),	//assumed to be running
mRegistryLocation("/home/marc/.rur/data/aim_registry.txt"),	//TODO: replace by using envir. path
mRegistryPortLocation("/home/marc/.rur/data/registry/")		//TODO: replace by using envir. path
{}

ModuleManager::~ModuleManager(){
	aimstop("all");
}

void ModuleManager::Init(){
	debug = true;
	loadModuleMap();	// read the registryfile and load all available modules
	ZMQInit();			// start up the zeroMQ ports
}

/*
void ModuleManager::loadModuleMap(){
	std::ifstream registryFile;
	std::string line;
	std::vector<std::string> tokens;		// Create vector to hold the individual words
	registryFile.open(mRegistryLocation);	// open file to read
	if (registryFile.is_open()){
		while (registryFile.good()){
			getline (registryFile,line);	// retrieve new line
			tokens = split_string(line);
			if (!line.empty() && tokens[1]=="=" && tokens.size()==3){
				addToUnactive(Module(tokens[0],tokens[2]));	//tokens[0] is moduleName
															//tokens[2] is moduleLocation
				loadAvailablePorts(tokens[0]);
			}
		}
		registryFile.close();
	}
	else {
		std::cout << "could not open " << mRegistryLocation << std::endl;
	}
}

void ModuleManager::loadAvailablePorts(std::string moduleName){
	std::ifstream portFile;
	std::string line;
	std::vector<std::string> tokens;
	portFile.open(mRegistryPortLocation + moduleName + ".ports");
	if (portFile.is_open()){
		while (portFile.good()){
			getline (portFile,line);	// retrieve new line
			tokens = split_string(line);
//					direction  name  middleware	 type
//			tokens 	   [0]		[1]		[2]		  [3]
			if(!line.empty() && tokens.size()==4 && (tokens[0]=="in" || tokens[0]=="out")){
				mUnactiveModuleMap.at(moduleName).addPort(ModulePort(tokens[1],tokens[0],tokens[3],tokens[2]));
			}
		}
	}
}
*/

void ModuleManager::loadModuleMap(){
	std::string line;
	std::vector<std::string> tokens;

	std::ifstream registryFile;
	registryFile.open(mRegistryLocation);

	if(registryFile.is_open()){
		while(registryFile.good()){
			getline (registryFile,line);	// retrieve new line
			tokens = split_string(line);
			if (!line.empty() && tokens[1]=="=" && tokens.size()==3){
				std::string location = tokens[2] + "/aim-core/aim_deployment.json";
				readJsonFile(location);
			}
		}
	}
}

void ModuleManager::readJsonFile(std::string location){
	std::ifstream file;
	if(debug){
		std::cout << "Reading JsonFile: " << location << std::endl;
	}
	file.open(location);
//	file.read();
	boost::property_tree::ptree pt;
	boost::property_tree::read_json(file, pt);
	Module module;
	bool success = module.read(pt);
	if(success){
		addToUnactive(module);
	}
}

void ModuleManager::addToUnactive(Module module){
	if(debug){
		std::cout << "adding unactive Module: " << module << std::endl;
	}
	mUnactiveModuleMap.insert({module.getName(),module});
}

bool ModuleManager::isPresent(std::string moduleName){
	return (mUnactiveModuleMap.count(moduleName)>0) ? true : false;
}

Module & ModuleManager::getUnactive(std::string moduleName){
        return mUnactiveModuleMap.at(moduleName);
}

void ModuleManager::addToActive(Module * module,int id){
        ActiveModule & active_module = *new ActiveModule(module);
        ModuleKey key = active_module.getKey();
        key.setID(id);
        active_module.setKeyID(id);
        mActiveModuleMap.insert({key,active_module});
}

bool ModuleManager::isActive(ModuleKey key){
        return (mActiveModuleMap.count(key)>0) ? true : false;
}

int ModuleManager::isActive(std::string moduleName){
	if(mUnactiveModuleMap.count(moduleName)>0){
		for(auto& x: mActiveModuleMap){
			if(x.first.getName()==moduleName){
//				return true;
				return x.first.getID();
			}
		}
//		return false;
		return -1;	// module on device, but not active
	}
	return -2;	//module not on device
}

ActiveModule & ModuleManager::getActiveModule(ModuleKey key){
        return mActiveModuleMap.at(key);
}

//=====================================================================================================================
//							AIM tools
//=====================================================================================================================
void ModuleManager::aimrun(Module & module, int id){
	ModuleKey key(module.getName(), id);
	if (isActive(key)) {
		std::cerr << "Module already active" << std::endl;
	} else{
		std::string command = "aimrun " + key.getName() + " " + std::to_string(key.getID()) + " &";
		std::cout << command << std::endl;

                int i=system(command.c_str());
                if (i==0){	// Add to active list if successfully started
                			// TODO: should be able to bind the module so I can remove it from the list if stopped externally
                        addToActive(&module,key.getID());
                }
        }
}

void ModuleManager::aimstop(ModuleKey key){
	std::string command = "aimstop standard "+ key.getName() + " " + std::to_string(key.getID());
	std::cout << command << std::endl;
	int i=system(command.c_str());
	if (i==0){	//Remove from list if successfully stopped
		mActiveModuleMap.erase(key);
	}
}

void ModuleManager::aimstop(std::string moduleName){
	for (auto& module: mActiveModuleMap){
		if (module.first.getName()==moduleName || moduleName=="all"){
			aimstop(module.first);
		}
	}
}

void ModuleManager::aimconnect(ModuleKey key1, ModuleKey key2,
		std::string portName1, std::string portName2,
		std::string device1, std::string device2){

	ModulePort port1,port2;
	port1 = ModulePort(portName1,"out");
	port2 = ModulePort(portName2,"in");

	std::string middleware;
	std::string command;

	//check if both modules are active locally
	bool bothlocal = (mActiveModuleMap.count(key1)>0 && mActiveModuleMap.count(key2));
	// check if exactly one module is (not) running locally
	bool just_one_local= (!(mActiveModuleMap.count(key1)>0) != !(mActiveModuleMap.count(key2)));

	//check if middleware of both modules is the same
//	if (mActiveModuleMap.at(key1).getModule().getMiddleWare()==mActiveModuleMap.at(key2).getModule().getMiddleWare()){

//		//check if middleware server is running
//		std::string middleware = mActiveModuleMap.at(key1).getModule().getMiddleWare();
//		if (middleware=="yarp"){
//			startYarpServer();
//		} else if(middleware=="zeromq"){
//			startZMQServer();
//		}

		if(bothlocal){
			middleware = mActiveModuleMap.at(key1).getModule().getMiddleWare();
			//connect the modules (the middlewares are still limited to being either yarp or zeromq)
			command = "aimconnect " +  middleware + " " +
				key1.getName() + " " + std::to_string(key1.getID()) + " " + port1.getName() + " " +
				key2.getName() + " " + std::to_string(key2.getID()) + " " + port2.getName() + " &";
			std::cout << mActiveModuleMap.at(key1).getModule().getMiddleWare() << std::endl;
			std::cout << mActiveModuleMap.at(key2).getModule().getMiddleWare() << std::endl;
			std::cout << command << std::endl;
			system(command.c_str());
			// insert both ends of the connection into the mActiveConnections list
			ModuleConnection tmp = ModuleConnection(port1,key1,port2,key2,mActiveModuleMap.at(key1).getModule().getMiddleWare());
			mActiveConnections.insert({port1,tmp});
			mActiveConnections.insert({port2,tmp});

		} else if(just_one_local){
			//check which module isn't local & whether it's port is input or output
			//port1 is assumed to be output (by design) and so, port2 is input
			std::cout << "identified only one module running locally" << std::endl;
			if (!(mActiveModuleMap.count(key1)>0)){	//first key is not local (output)
				// connect local module to dodedodoServer (the 'Server' name is hardcoded in ZMQInit())
				std::string moduleName = key2.getName();
				std::transform(moduleName.begin(),moduleName.end(),moduleName.begin(), ::tolower);
				command = "zmqconnect /dodedodoServer/output /" +
						moduleName + std::to_string(key2.getID()) + "/" + port2.getName() + " &";

//				AIM connect raspberrypi DodedodoModule 0 portaudio raspberrypi testModule 0 portinput

			} else if(!(mActiveModuleMap.count(key2)>0)){ // second key is not local (input)
				// connect local module to dodedodoServer (the 'Server' name is hardcoded in ZMQInit())
				std::string moduleName = key1.getName();
				std::transform(moduleName.begin(),moduleName.end(),moduleName.begin(), ::tolower);
				command = "zmqconnect /" +
						moduleName + std::to_string(key1.getID()) + "/" + port1.getName() + " /dodedodoServer/output &";
			} else {
				//shouldn't happen, error
			}
			std::cout << command << std::endl;
			system(command.c_str());
			// add the connection to the mActiveConnections list
			middleware = "zeromq";
			ModuleConnection tmp = ModuleConnection(port1,key1,device1,port2,key2,device2,middleware);
			mActiveConnections.insert({port1,tmp});
			mActiveConnections.insert({port2,tmp});

		}
//	}	// middleware check
}

void ModuleManager::aimdisconnect(ModulePort port) {
	//TODO
	//check if port is connected in 'mActiveConnections' list
	if(mActiveConnections.count(port)>0){
		// find the corresponding other entry in the list
		//actually disconnect the ports
	}
}

void ModuleManager::aimdataToLocal(ModuleKey key,ModulePort port,std::vector<int> data){
	//send incoming data over zmq port to correct Module n' port
	// TODO: check which local module & port is linked to which outgoing port
//	myZMQports->writeOutGoing(const long_seq output);
	myZMQports->writeOutGoing(data);
}

void ModuleManager::aimdataToLocal(ModuleKey key,ModulePort port,std::vector<float> data){
//	myZMQports->writeOutGoing()
}

void ModuleManager::aimdataToLocal(ModuleKey key,ModulePort port,std::string data){
	myZMQports->writeOutGoing(data);
}

boost::property_tree::ptree ModuleManager::aimstatus(std::string moduleName){
	boost::property_tree::ptree pt;
	// check if active & call other aimstatus
	//if not active assign id = -1
	int id = isActive(moduleName);
	if(id==-2){ //module not present or registered on device
		pt.put("name",moduleName);
		pt.put("present on device","NO!");
	} else {
		pt = aimstatus(ModuleKey(moduleName,id));
	}
	return pt;
}

boost::property_tree::ptree ModuleManager::aimstatus(ModuleKey key){
	boost::property_tree::ptree pt,ports;
	pt.put("name",key.getName());
	pt.put("ID",std::to_string(key.getID()));
	pt.put("active",isActive(key));

	for (auto& p: mUnactiveModuleMap.at(key.getName()).getPortMap()){
		ports.push_back(std::make_pair("",aimstatus(key,p.second)));
	}
	pt.put_child("ports",ports);
	return pt;
}

boost::property_tree::ptree ModuleManager::aimstatus(ModuleKey key,ModulePort port){
	boost::property_tree::ptree porttree;
	porttree.put("name",port.getName());
	if (mActiveConnections.count(port)>0){
		porttree.put("connected",true);
	} else {
		porttree.put("connected",false);
	}
	return porttree;
}

void ModuleManager::aimdeploy(Module module){
	if(debug){
		std::cout << "deploying new module....when this is fully implemented" << std::endl;//TODO
	}
}

//=====================================================================================================================
//						NameServer Function
//=====================================================================================================================
void ModuleManager::startYarpServer(){
	if(!mYarpServRunning){	// TODO add a check that searches for the running process
		system("yarp_server &");
		mYarpServRunning = true;
	}
}

void ModuleManager::startZMQServer(){
	if (!mZMQServRunning){	// TODO add a check that searches for the running process
		system("zmqserver &");
		mZMQServRunning = true;
	}
}

void ModuleManager::stopYarpServer(){
//	TODO: improve beyond bare functionality
	system("killall yarp");
	mYarpServRunning = false;
}

void ModuleManager::stopZMQServer(){
	if(mZMQServRunning){
		system("killall zeromqserver");
		mZMQServRunning = false;
	}
}

void ModuleManager::ZMQInit(){
	std::string name = "Server";
	myZMQports->Init(name);
}

std::ostream& operator<<(std::ostream& os, const ModuleManager& xec){
	os << "Available Modules: " << std::endl;
	for (auto& x: xec.mUnactiveModuleMap){
		os << x.first << std::endl << x.second << std::endl;
	}
	os << "Active Modules: " << std::endl;
	for (auto& x: xec.mActiveModuleMap){
		os << x.second;
	}
	return os;
}

