/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief
 * @file xmpp_service.cpp
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    Marc Hulscher
 * @date      Jun 14, 2013
 * @project   Dodedodo
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 */

#include "xmpp_service.h"
#include "line_split.h"

#include <boost/bind.hpp>
#include <boost/property_tree/ptree.hpp>		//JSON
#include <boost/property_tree/json_parser.hpp>	//JSON

using namespace Swift;

int main() {
	std::cout << "Dodedodo app for Raspberry Pi" << std::endl;

	XMPPService servicetest;

	return 0;
}

XMPPService::XMPPService(): debug(true){

	char* pUsername = getenv("XMPP_NAMESERVER_USERNAME");
	if (pUsername == NULL) {
		std::cerr << "Could not retrieve username from env. variable " << std::endl;
		exit(EXIT_FAILURE);
	}

	char* pPassword = getenv("XMPP_NAMESERVER_PASSWORD");
	if (pPassword == NULL) {
		std::cerr << "Could not retrieve password from env. variable " << std::endl;
		exit(EXIT_FAILURE);
	}
	char* pResource = getenv("XMPP_NAMESERVER_RESOURCE");
	if (pResource == NULL) {
		std::cerr << "Could not retrieve resource name from env. variable " << std::endl;
		exit(EXIT_FAILURE);
	}

	char* pHostName = getenv("XMPP_NAMESERVER_HOST");
	if (pHostName == NULL)
		pHostName = "@dobots.customers.luna.net";

	char* pAdminJID = getenv("XMPP_ADMIN_USERNAME");
	if (pAdminJID == NULL)
		pAdminJID = "hal9000@dobots.customers.luna.net";

	std::string jid = std::string(pUsername) + "@" + std::string(pHostName) + "/" + std::string(pResource);
	mJID = Swift::JID(jid);
	mPassword = std::string(pPassword);

//	mModuleManager = ModuleManager();		// start modulemanager
	mModuleManager.Init();


	mNetworkFactories = new Swift::BoostNetworkFactories(&mEventLoop);
	mClient = new Swift::Client(mJID, mPassword, mNetworkFactories);
	mClient->setAlwaysTrustCertificates(); // TODO: this is bad!!

	// bind callback functions
	mClient->onConnected.connect(boost::bind(&XMPPService::handleConnected, this));
	mClient->onPresenceReceived.connect(boost::bind(&XMPPService::handlePresenceReceived, this, _1));
	mClient->onMessageReceived.connect(boost::bind(&XMPPService::handleXMPPMessageReceived, this, _1));
	mClient->getEntityCapsProvider()->onCapsChanged.connect(boost::bind(&XMPPService::handleCapsChanged, this, _1));

	mClient->setSoftwareVersion("Dodedodo","v0.1","raspbian");

	// debugging tool mTracer dumps all XMPP messages to console
	if (debug){
		std::cout << mModuleManager << std::endl;
		mTracer = new Swift::ClientXMLTracer(mClient);
	}

	//	mSoftwareVersionResponder = new Swift::SoftwareVersionResponder(mClient->getIQRouter());
	//	mSoftareVersionResponder->setVersion("Dodedodo", "1.0");
	//	mSoftwareVersionResponder->start();

	XMPPConnect();

	mFileTransferManager = mClient->getFileTransferManager();
}

XMPPService::~XMPPService() {
	delete mNetworkFactories;
	mClient->onConnected.disconnect(boost::bind(&XMPPService::handleConnected, this));
	mClient->onPresenceReceived.disconnect(boost::bind(&XMPPService::handlePresenceReceived, this, _1));
	mClient->onMessageReceived.disconnect(boost::bind(&XMPPService::handleXMPPMessageReceived, this, _1));
	mClient->getEntityCapsProvider()->onCapsChanged.disconnect(boost::bind(&XMPPService::handleCapsChanged, this, _1));
	delete mClient;
	if (debug){
		delete mTracer;
	}
	delete mFileTransferManager;
}

bool XMPPService::XMPPConnect(){
	mClient->connect();
	mEventLoop.run();
	return true;
}

bool XMPPService::XMPPDisconnect(){
	mClient->disconnect();
	return true;
}

bool XMPPService::XMPPSend(JID jid,std::string body){
	if (!mClient->isAvailable()){
		return false;
	} else {
		Swift::Message::ref message (new Swift::Message);
		message->setTo(jid);
		message->setBody(body);
		message->setType(Swift::Message::Chat);
		mClient->sendMessage(message);
		return true;
	}
}

bool XMPPService::XMPPSend(std::string to,std::string body){
	return XMPPSend(Swift::JID(to),body);
}

void XMPPService::handleConnected() {
	std::cout << "Connected" << std::endl;
	Swift::GetRosterRequest::ref rosterRequest = Swift::GetRosterRequest::create(mClient->getIQRouter());
	rosterRequest->onResponse.connect(boost::bind(&XMPPService::handleRosterReceived, this, _2));
	rosterRequest->send();
}

void XMPPService::handleRosterReceived(Swift::ErrorPayload::ref error) {
	std::cout << "Roster received" << std::endl;
	if (error) {
		std::cerr << "Error receiving roster. Continuing anyway.";
	}
	// Send initial available presence
	mClient->sendPresence(Swift::Presence::create("Online"));
}

void XMPPService::handlePresenceReceived(Swift::Presence::ref presence) {
	std::cout << "Presence received" << std::endl;
	if (presence->getType() == Swift::Presence::Subscribe) {
		Swift::Presence::ref response = Swift::Presence::create();
		response->setTo(presence->getFrom());
		response->setType(Swift::Presence::Subscribed);
		mClient->sendPresence(response);
	}
}

void XMPPService::handleCapsChanged(Swift::JID jid) {
	std::cout << jid << std::endl;
}

void XMPPService::handleXMPPMessageReceived(Swift::Message::ref message) {
	std::vector<std::string> tokens = split_string(message->getBody());
	if (message->getBody().size() > 0 && tokens[0]=="AIM" && tokens.size()>2) {
		std::string letdown = "not yet implemented: AIM " + tokens[1];
		std::string invalid = "invalid AIM message";

		if (tokens[1] == "data" && tokens.size() >= 7){
			// Leave it up to the modules to check if nDim and sizeDN match with array size

			// 		  "AIM" "data" int/float moduleName id port nDim sizeD1 sizeD2 .. data
			//		  "AIM" "data" string 	 moduleName id port some text   here
			// tokens  [0]    [1]	 [2]	   [3]		[4] [5]  [6]  [7]    [8]	[8+]

			std::string& name = tokens[3];
			int id = atoi(tokens[4].c_str());
			std::string& port = tokens[5];

			if (tokens[2]=="int"){
				// define int vector
				// first value in the array describes nDim (number of dimensions)
				// the following nDim values describe the sizes of dimensions
				// the remaining values describe the

				std::vector<int> data(tokens.size()-6);
				for (unsigned int i=6;i<tokens.size();i++){
					data[i-6] = atoi(tokens[i].c_str());
				}

				// Better?
//				std::vector<std::string>::const_iterator itIn = tokens.begin();
//				itIn += 6;
//				std::vector<int> data(tokens.size()-6);
//				std::vector<int>::iterator itOut = data.begin();
//				for (;itIn != tokens.end(); itIn++, itOut++){
//					*itOut = atoi(itIn->c_str());
//				}

				mModuleManager.aimdataToLocal(ModuleKey(name, id), ModulePort(port,"in"), data);

			} else if (tokens[2]=="float"){
				//define float array
				std::vector<float> data(tokens.size()-6);
				for (unsigned int i=6;i<tokens.size();i++){
					data[i-6] = atof(tokens[i].c_str());
				}
				mModuleManager.aimdataToLocal(ModuleKey(name, id), ModulePort(port,"in"), data);

			} else if (tokens[2]=="string"){
				size_t pos=6; // to account for spaces in between
				for (size_t i=0; i<6; ++i)
					pos += tokens[0].size();
				std::string data = message->getBody().substr(pos);
				mModuleManager.aimdataToLocal(ModuleKey(name, id), ModulePort(port,"in"), data);

			} else{
				if (debug)
					XMPPSend(message->getFrom(),invalid); // AIM message is invalid if data type isn't an int/float/string
			}

		} else if (tokens[1] == "deploy"){
			//should be provided with module description
			//        "AIM" "deploy" module_description (json msg)
			// tokens  [0]    [1]        ...[?]
			std::stringstream ss(message->getBody().substr(11));
			boost::property_tree::ptree pt;
			boost::property_tree::read_json(ss, pt);
			Module tmp;
			bool success = tmp.read(pt);
			mModuleManager.aimdeploy(tmp);
			if (debug) {
				if (success){
					XMPPSend(message->getFrom(),"successfully received module description");
				} else {
					XMPPSend(message->getFrom(),"something went wrong, try again please");
				}
			}

		} else if (tokens[1] == "start" && tokens.size() >= 4 && isdigit(tokens[3].at(0))) {
			//       "AIM" "start" moduleName id
			// tokens [0]    [1]      [2]     [3]
			std::string& name = tokens[2];
			if (mModuleManager.isPresent(tokens[2])){
				int id = atoi(tokens[3].c_str());
				mModuleManager.aimrun(mModuleManager.getUnactive(name), id);
				if (debug)
					XMPPSend(message->getFrom(), "module started");
			} else{
				std::cout << "module not present on device: " << name << std::endl;
			}

		} else if (tokens[1] == "connect" && tokens.size()==10) {
			//       "AIM" "connect"  device moduleName id  port_out device moduleName id   port_in
			// tokens [0]      [1]      [2]      [3]    [4]    [5]     [6]      [7]    [8]    [9]
			std::string& deviceOut = tokens[2];
			std::string& nameOut = tokens[3];
			int idOut = atoi(tokens[4].c_str());
			std::string& portOut = tokens[5];
			std::string& deviceIn = tokens[6];
			std::string& nameIn = tokens[7];
			int idIn = atoi(tokens[8].c_str());
			std::string& portIn = tokens[9];

			mModuleManager.aimconnect(ModuleKey(nameOut, idOut),
					ModuleKey(nameIn, idIn), portOut, portIn, deviceOut, deviceIn);
		} else if (tokens[1] == "disconnect") {
			//       "AIM" "disconnect" moduleName id  portName
			// tokens [0]      [1]        [2]      [3]    [4]
			if (debug)
				XMPPSend(message->getFrom(), letdown);
		} else if (tokens[1] == "stop" && tokens.size()>=3) { //&& isdigit(tokens[3].at(0))
			//        "AIM" "stop" moduleName id
			// tokens  [0]    [1]     [2]     [3]
			std::string& name = tokens[2];
			int id = atoi(tokens[3].c_str());
			ModuleKey key(name, id);
			if(name=="all"){
				mModuleManager.aimstop("all");
				XMPPSend(message->getFrom(),"stopping all running modules");
			}else if (mModuleManager.isActive(key)){ //TODO: make this prettier
				mModuleManager.aimstop(key);
				if (debug)
					XMPPSend(message->getFrom(), "module stopped");
			}else{
				std::cout << "module not running: " << key << std::endl;
			}
		} else if (tokens[1] == "uninstall") {
			// 	     "AIM" "uninstall" moduleName
			// tokens [0]      [1]        [2]
			if (debug)
				XMPPSend(message->getFrom(), letdown);

		} else if (tokens[1] == "status" && tokens.size()>=3) {
			// obtaining the status of a single port is not yet supported
			//       "AIM" "status" moduleName id 	portName
			// tokens [0]     [1]       [2]    [3]     [4]
			boost::property_tree::ptree pt;
			std::stringstream ss;


			if (tokens.size() > 2) {
				std::string& name = tokens[2];
				if (tokens.size() > 3) {
					int id = atoi(tokens[3].c_str());
					ModuleKey key(name, id);
					pt = mModuleManager.aimstatus(key);
					if (tokens.size() > 4) {
						std::string& port = tokens[4];
						//	XMPPSend(message->getFrom(),letdown);
					}
					else
						pt = mModuleManager.aimstatus(key);
				} else
					pt = mModuleManager.aimstatus(name);
			}

			boost::property_tree::json_parser::write_json(ss, pt, true);
			XMPPSend(message->getFrom(), ss.str());
		} else {
			if (debug)
				XMPPSend(message->getFrom(), invalid);
		}
		//		std::cout << "Message received: " << message->getBody() << std::endl;
		//		XMPPSend("marc@dobots.customers.luna.net","hoi van de client");	// TODO: Remove this later
	} else {
		if (debug)
			XMPPSend(message->getFrom(), "invalid AIM message");
	}
}
