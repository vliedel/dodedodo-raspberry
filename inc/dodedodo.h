/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief
 * @file dodedodo.h
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    Marc Hulscher
 * @date      Jun 14, 2013
 * @project   Dodedodo
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
  */

#ifndef DODEDODO_H_
#define DODEDODO_H_

#include <string>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdexcept>
#include <vector>
#include <sstream>
#include <unordered_map>	//possible due to addition of -std=c++0x flag

#include <boost/property_tree/ptree.hpp>		//JSON
#include <boost/property_tree/json_parser.hpp>	//JSON

#include "active_module.h"
#include "module.h"
#include "moduleconnection.h"
#include "moduleport.h"
#include "line_split.h"
#include "zeromq.h"

class ModuleManager {
private:
	std::unordered_map<ModuleKey, ActiveModule> mActiveModuleMap; 	// Only used for activated modules (map key is modulekey)
	std::unordered_map<std::string, Module> mUnactiveModuleMap;	// Used to list unactive/available modules (map key = mModuleName)
	std::unordered_map<ModulePort,ModuleConnection> mActiveConnections; // (map key = mThisPort)
	// A connection is inserted twice into the mActiveConnections list. Once for each end of the connection
	bool debug;
	bool mYarpServRunning;
	bool mZMQServRunning;
	zmqports *myZMQports = new zmqports();	// Right now we have a default input and output port that accept and send int vectors
	std::string mRegistryLocation;
	std::string mRegistryPortLocation;
/*
	void systemCall(std::string command){
		if(debug){
			std::cout << command << std::endl;
		}
		system(command.c_str());
}

	bool systemCheckIfRunning(std::string moduleName){
		std::string = "pgrep " + name + " > /dev/null";
		return 0 == system(command.c_str());
	}
*/
public:
	ModuleManager();
	~ModuleManager();

	void Init();
	/*	=============================
	 *	mUnactiveModuleMap functions
	 */
	void loadModuleMap();							//TODO: Might make this a boolean to reflect success of action
//	void loadAvailablePorts(std::string moduleName);//TODO: Might make this a boolean
	void readJsonFile(std::string location);
	void addToUnactive(Module module);				//TODO: Might make this a boolean
	bool isPresent(std::string moduleName);			// checks if module is installed
	Module & getUnactive(std::string moduleName);

	/*	==========================
	 *	mActiveModuleMap functions
	 */
	void addToActive(Module * module,int id);		//TODO: Might make this a boolean
	bool isActive(ModuleKey key);					// checks if moduleKey is in active list
	int isActive(std::string moduleName);			// checks is a module is active
	ActiveModule & getActiveModule(ModuleKey key);	//TODO: remove this function?

	/*	==================
	 * 	AIMtools functions
	 */
	void aimrun(Module & module,int id);		// id assigned by the server
	void aimstop(ModuleKey key);				// stops specific module instance
	void aimstop(std::string moduleName);		// stops all instances of module with moduleName or "all"
	void aimconnect(ModuleKey key1, ModuleKey key2, std::string portName1, std::string portName2,std::string device1, std::string device2);
	void aimdisconnect(ModulePort port);		// TODO: should accept module ID & port
	void aimdataToLocal(ModuleKey key,ModulePort port,std::vector<int> data);
	void aimdataToLocal(ModuleKey key,ModulePort port,std::vector<float> data);
	void aimdataToLocal(ModuleKey key,ModulePort port,std::string data);

//	boost::property_tree::ptree aimstatus(std::string name);// return JSON with information on all active modules with
															// name "name"
	boost::property_tree::ptree aimstatus(std::string moduleName);
	boost::property_tree::ptree aimstatus(ModuleKey key);
	boost::property_tree::ptree aimstatus(ModuleKey key,ModulePort port);

	void aimdeploy(Module module);	// download the module from repo, aimselect (zmq) & aimregister
//	void aimuninstall(Module module);
//	void aimlist();

	/*	====================
	 *	NameServer Functions
	 */
	void startYarpServer();
	void stopYarpServer();
	void startZMQServer();
	void stopZMQServer();
	void ZMQInit();

	friend std::ostream& operator<<(std::ostream& os, const ModuleManager& xec);
};



#endif /* DODEDODO_H_ */
