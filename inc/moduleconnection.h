/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file moduleconnection.h
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 11, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#ifndef MODULECONNECTION_H_
#define MODULECONNECTION_H_

#include <stdlib.h>
#include <string>
#include "module.h"

class ModuleConnection{
private:
	std::string mThisDevice;	// defaults to "Raspberry_Pi"
	ModulePort mThisPort;
	ModuleKey mThisModuleKey;
//	Module mThisModule;
	std::string mOtherDevice;	// defaults to "Raspberry_Pi"
	ModulePort mOtherPort;
	ModuleKey mOtherModuleKey;
//	Module otherModule;
	std::string mMiddleWare;	// defaults to zeromq, other options are: yarp etc..
public:
	ModuleConnection();
	ModuleConnection(ModulePort thisPort,ModuleKey thisKey,ModulePort otherPort,ModuleKey otherKey);
	ModuleConnection(ModulePort thisPort,ModuleKey thisKey,ModulePort otherPort,ModuleKey otherKey,std::string middleware);
	ModuleConnection(ModulePort thisPort,ModuleKey thisKey,std::string thisDevice,ModulePort otherPort,ModuleKey otherKey,std::string otherDevice,std::string middleware="zeromq");
	~ModuleConnection();
	void setMiddleWare(std::string middleware);
	void Connect();
	void Disconnect();
};

#endif /* MODULECONNECTION_H_ */
