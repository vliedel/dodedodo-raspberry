/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief 
 * @file modules.h
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    marc
 * @date      Sep 6, 2013
 * @project   
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
 * @case      
 */

#ifndef MODULES_H_
#define MODULES_H_

#include <string>
#include <iostream>
#include <unordered_map>	//possible due to addition of -std=c++0x flag

#include <modulekey.h>
#include <moduleport.h>

#include <boost/property_tree/ptree.hpp>

struct Android {
	std::string mUrl;
	std::string mPackageName;
};

struct Git {
	std::string mUrl;
};

class Module {
private:
	std::string mModuleName;
	std::string mDeviceName;
	std::string mModuleDescription;
	bool mEnable;
	std::string mModuleLocation;
	std::string mMiddleWare;	// TODO: change to enum or something (limit possibilities)
	std::unordered_map<std::string, ModulePort> mPorts;		// port name is map key
	std::string mType; // currently UI or background
	Android mAndroid;
	Git mGit;
	bool mRegistered;
public:
	Module();
	Module(std::string moduleName, std::string moduleLocation);
	~Module(){}

	std::string getName() const;


	void addPort(std::string portName,std::string portDirection);
	/**
	 * Add port. If middleware left unset, it will get the middleware of the module itself.
	 */
	void addPort(std::string portName,std::string portDirection,std::string portType,std::string portMiddleWare="");
	void addPort(ModulePort port);

	std::unordered_map<std::string, ModulePort> getPortMap() const;

	inline void setResource(std::string resource) { mModuleLocation = resource; }

	//! Read in a property tree
	bool read(boost::property_tree::ptree & pt);

	//! Write a property tree
	void write(boost::property_tree::ptree & pt);

	inline bool registered() { return mRegistered; }

	inline void set_registered(bool registered=true) { mRegistered = registered; }

	std::string getMiddleWare() const;
	friend std::ostream& operator<<(std::ostream& os, const Module& module);
};

#endif /* MODULES_H_ */
