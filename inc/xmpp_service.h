/**
 * 456789------------------------------------------------------------------------------------------------------------120
 *
 * @brief
 * @file xmpp_service.h
 *
 * This file is created at Almende B.V. and Distributed Organisms B.V. It is open-source software and belongs to a
 * larger suite of software that is meant for research on self-organization principles and multi-agent systems where
 * learning algorithms are an important aspect.
 *
 * This software is published under the GNU Lesser General Public license (LGPL).
 *
 * It is not possible to add usage restrictions to an open-source license. Nevertheless, we personally strongly object
 * against this software being used for military purposes, factory farming, animal experimentation, and "Universal
 * Declaration of Human Rights" violations.
 *
 * Copyright (c) 2013 Marc J. Hulscher <marc@dobots.nl>
 *
 * @author    Marc Hulscher
 * @date      Jun 14, 2013
 * @project   Dodedodo
 * @company   Almende B.V.
 * @company   Distributed Organisms B.V.
  */

#ifndef XMPP_SERVICE_H_
#define XMPP_SERVICE_H_

#include <string>
#include <Swiften/Swiften.h>
#include "dodedodo.h" // ModuleManager class

class XMPPService{
private:
	bool debug;

	std::string mPassword;
	Swift::JID mJID;
	Swift::SimpleEventLoop mEventLoop;
	Swift::BoostNetworkFactories *mNetworkFactories;
	Swift::Client* mClient;
	Swift::ClientXMLTracer* mTracer;

	ModuleManager mModuleManager;

	Swift::FileTransferManager* mFileTransferManager;

	bool XMPPSend(Swift::JID jid,std::string body);
	bool XMPPSend(std::string to,std::string body);
//	bool XMPPSend(std::string to,std::string body,Swift::Message::Type type);	//TODO

	// Callbacks
	void handleConnected();
	void handleRosterReceived(Swift::ErrorPayload::ref error);
	void handlePresenceReceived(Swift::Presence::ref presence);
	void handleCapsChanged(Swift::JID jid);	//TODO: empty for now
	void handleXMPPMessageReceived(Swift::Message::ref message);

//	void handleModuleMessageReceived();

//	void msgSend(Swift::Message::ref message);
//	void startModule(ModuleKey key);
//	void stopModule(ModuleKey key);
//	void setMessenger(ModuleKey keyIn, String portIn)
//	void connect(String deviceOut, ModuleKey keyOut, String portOut, String deviceIn, ModuleKey keyIn, String portIn)
//	void getMessengers();
//	void storeModuleMap();
//	void loadModuleMap();
//	void onServiceDisconnected(ComponentName className);
//	void doBindXmppService();
//	void doUnbindXmppService();


public:
	XMPPService();
	~XMPPService();
	bool XMPPConnect();
	bool XMPPDisconnect();
};



#endif /* XMPP_SERVICE_H_ */
