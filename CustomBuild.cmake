#######################################################################################################################
# ___________________________________________________________________________________________________________________ #
# ___________________________________________/\/\______/\/\/\/\__/\/\______/\/\______________________________________ #
# _________________________________________/\/\/\/\______/\/\____/\/\/\__/\/\/\______________________________________ #
# _______________________________________/\/\____/\/\____/\/\____/\/\/\/\/\/\/\______________________________________ #
# _______________________________________/\/\/\/\/\/\____/\/\____/\/\__/\__/\/\______________________________________ #
# _______________________________________/\/\____/\/\__/\/\/\/\__/\/\______/\/\______________________________________ #
# ___________________________________________________________________________________________________________________ #
#                                                                                                                     #
#######################################################################################################################

# Swiften 
#   Boost libraries: signals, thread, regex, program_options, filesystem, system, date_time
#   Miscellaneous: idn, z, ssl, crypto, xml2, avahi-client, avahi-common, sqlite3, resolv, pthread, dl, m, c, stdc++

# This will retrieve all dependencies
FIND_PACKAGE(ZMQ REQUIRED)
INCLUDE_DIRECTORIES(${ZMQ_INCLUDE_DIRS})
SET(LIBS ${LIBS} ${ZMQ_LIBRARIES})

FIND_PACKAGE(JsonSpirit REQUIRED)
INCLUDE_DIRECTORIES(${JSON_SPIRIT_INCLUDE_DIRS})
SET(LIBS ${LIBS} ${JSON_SPIRIT_LIBRARIES})

# Currently tacitly assuming rt is always there...
SET(LIBS ${LIBS} rt)

FIND_PACKAGE(Swiften)
SET(LIBS ${LIBS} ${SWIFTEN_LIBRARY})

IF(UNIX)
	SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=gnu++0x")
ENDIF()

