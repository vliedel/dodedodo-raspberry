# Dodedodo-Raspberry

## What does it do?

The Dodedodo-Raspberry app acts as a manager for AIM modules on a Raspberry Pi. It receives commands from the Dodedodo server over an XMPP connection.
These commands are then parsed by the app to affect the modules running on the Pi.

## compiling the code on your pi

first install the following dependencies
- Swiften
- Boost property tree (http://stackoverflow.com/questions/18552427/boost-read-json-and-c11)

